package com.sap.cloud.sdk.tutorial;

import lombok.Data;
 
import com.sap.cloud.sdk.result.ElementName;
 
@Data
public class CostCenterDetails
{
    @ElementName( "CostCenterID" )
    private String costCenterID;
 
    @ElementName( "CompanyCode" )
    private String companyCode;
 
    @ElementName( "Status" )
    private String status;
 
    @ElementName( "Category" )
    private String category;
 
    @ElementName( "CostCenterDescription" )
    private String costCenterDescription;
}